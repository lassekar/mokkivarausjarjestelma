package majoitusvarausjarjestelma;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import majoitusvarausjarjestelma.Oliot.Asiakas;


public class frmAsiakas extends JFrame{
	
	//m��ritell��n k�ytt�liittym�n objektit
	private JLabel lblValikko;
	private JButton btnUusiAsiakas;
	private JButton btnMuokkaaAsiakas;
	private JButton btnPoistaAsiakas;
	private JButton btnTakaisin;
	private JButton btnSelaaAsiakas;
	private JTextArea areaAsiakas;
	private JPanel pn1;
	private JSeparator sep;
	private JScrollPane areaWithScroll;
	private JFrame f;
	
	public frmAsiakas() {
		tuoVieKomponentitPaikalleen();
		
		
		
		//lis�t��n lopetuspainikkeelle kuuntelija
				btnTakaisin.addActionListener(
						new ActionListener() {
							public void actionPerformed (ActionEvent actEvent) {
								setVisible(false);
							}
						});
				
				btnSelaaAsiakas.addActionListener(
						new ActionListener() {
							public void actionPerformed (ActionEvent actEvent) {
							}
						});
				
				
		add(pn1);
		setLocation (100,100);//ikkunan paikka
		setSize(900, 600); //ikkunan koko
		setTitle("Majoitusvarausj�rjestelm�");
		setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE); //Frame sulkeutuu, mutta ohjelma silti j�� k�yntiin
		
	}
	
	private void tuoVieKomponentitPaikalleen() {
		//komponenttien luonti loppuun
		this.lblValikko = new JLabel("ASIAKKAAT"); 
		this.btnUusiAsiakas = new JButton("Luo uusi asiakas");
		this.btnMuokkaaAsiakas = new JButton("Muokkaa asiakkaan tietoja");
		this.btnPoistaAsiakas = new JButton("Poista asiakas");
		this.btnTakaisin = new JButton("Takaisin");
		this.btnSelaaAsiakas = new JButton("Selaa asiakkaita");
		this.areaAsiakas = new JTextArea();
		this.areaAsiakas.setRows(20);
		this.sep = new JSeparator(SwingConstants.HORIZONTAL);
		this.pn1 = new JPanel();
		this.pn1.setLayout(new GridBagLayout());
		this.areaWithScroll = new JScrollPane(areaAsiakas);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		/*OHJEITA:
		 *gridx = m��ritt�� komponentin paikan rivill� eli sarakkeen sill� rivill�
		 *gridy = m��ritt�� sen mill� rivill� komponentti on
		 *weightx = m��ritt�� miten extra tila jaetaan paneelissa
		 *gridwidth = m��ritt��, kuinka pitk�lle komponentti vaakasuoraan menee rivill�
		 *ipady = lis�� sis�ist� "t�ytett�" eng. internal padding
		 *insets = lis�� m��rittelyn verran (yl�s, vasemmalle, alas, oikealle) "tyhj��"*/
		
		//Valikon painikkeet
		gbc.gridwidth = 2;
		gbc.weightx = 4;
		gbc.gridx = 0;
		gbc.gridy = 0;
		pn1.add(lblValikko,gbc);
		
		gbc.weightx = 1;
		gbc.gridx = 2;
		pn1.add(btnUusiAsiakas,gbc);
		
		gbc.gridx = 4;
		pn1.add(btnMuokkaaAsiakas,gbc);
		
		gbc.gridx = 6;
		pn1.add(btnPoistaAsiakas,gbc);
		
		gbc.gridx = 8;
		pn1.add(btnTakaisin,gbc);

		gbc.ipady = 40;
		gbc.weightx = 0;
		gbc.gridwidth = 12;
		gbc.gridx = 0;
		gbc.gridy = 1;
		pn1.add(sep,gbc);
		
		
		gbc.ipady = 0;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 2;
		pn1.add(btnSelaaAsiakas,gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 8;
		gbc.insets = new Insets(10,0,0,0);
		pn1.add(areaWithScroll, gbc);
		
	
		
	}
	public static void main(String[] args) {
		//avataan frame
		frmAsiakas frm = new frmAsiakas();
		frm.setVisible(true);
	}

}