package majoitusvarausjarjestelma;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MonkkonenE
 */
import java.sql.*;
import java.lang.*;
public class Asiakas {
		
	private int asiakasnumero;
	private String etunimi;
	private String sukunimi;
	private String puhelinnumero;
	private String katuosoite;
	private String paikkakunta;
	private String postinumero;
	private String maa;
			
	public Asiakas(){
		
	}
	
	public int getAsiakasnumero() {
		return asiakasnumero;
	}

	public void setAsiakasnumero(int asiakasnumero) {
		this.asiakasnumero = asiakasnumero;
	}

	public String getEtunimi() {
		return etunimi;
	}

	public void setEtunimi(String etunimi) {
		this.etunimi = etunimi;
	}

	public String getSukunimi() {
		return sukunimi;
	}

	public void setSukunimi(String sukunimi) {
		this.sukunimi = sukunimi;
	}

	public String getPuhelinnumero() {
		return puhelinnumero;
	}

	public void setPuhelinnumero(String puhelinnumero) {
		this.puhelinnumero = puhelinnumero;
	}

	public String getKatuosoite() {
		return katuosoite;
	}

	public void setKatuosoite(String katuosoite) {
		this.katuosoite = katuosoite;
	}

	public String getPaikkakunta() {
		return paikkakunta;
	}

	public void setPaikkakunta(String paikkakunta) {
		this.paikkakunta = paikkakunta;
	}

	public String getPostinumero() {
		return postinumero;
	}

	public void setPostinumero(String postinumero) {
		this.postinumero = postinumero;
	}

	public String getMaa() {
		return maa;
	}

	public void setMaa(String maa) {
		this.maa = maa;
	}
	
	public String toString() {
		return asiakasnumero + ", " + etunimi + ", " + sukunimi + ", " + puhelinnumero + ", " + katuosoite + ", " + paikkakunta + ", " + postinumero + ", " + maa;
	}

	/*
	Haetaan asiakkaan tiedot ja palautetaan asiakasolio kutsujalle.
	Staattinen metodi, ei vaadi fyysisen olion olemassaoloa.
	*/
	public static Asiakas haeAsiakas (Connection connection, int id) throws SQLException, Exception { // tietokantayhteys v�litet��n parametrina
		// haetaan tietokannasta asiakasta, jonka asiakas_id = id 
		String sql = "SELECT Asiakasnumero, Sukunimi, Etunimi, Puhelinnumero, Katuosoite, Paikkakunta, Postinumero, Maa " 
					+ " FROM Asiakas WHERE Asiakasnumero = ?"; // ehdon arvo asetetaan j�ljemp�n�
		ResultSet tulosjoukko = null;
		PreparedStatement lause = null;
		try {
			// luo PreparedStatement-olio sql-lauseelle
			lause = connection.prepareStatement(sql);
			lause.setInt( 1, id); // asetetaan where ehtoon (?) arvo
			// suorita sql-lause
			tulosjoukko = lause.executeQuery();	
			if (tulosjoukko == null) {
				throw new Exception("Asiakasta ei loydy");
			}
		} catch (SQLException se) {
            // SQL virheet
                        throw se;
                } catch (Exception e) {
            // JDBC virheet
                        throw e;
		}
		// k�sitell��n resultset - laitetaan tiedot asiakasoliolle
		Asiakas asiakasOlio = new Asiakas ();
		
		try {
			if (tulosjoukko.next () == true){
				//asiakas_id, etunimi, sukunimi, lahiosoite, postitoimipaikka, postinro, email, puhelinnro
				asiakasOlio.setAsiakasnumero (tulosjoukko.getInt("Asiakasnumero"));
				asiakasOlio.setSukunimi (tulosjoukko.getString("Sukunimi"));
				asiakasOlio.setEtunimi(tulosjoukko.getString("Etunimi"));
				asiakasOlio.setPuhelinnumero (tulosjoukko.getString("Puhelinnumero"));
				asiakasOlio.setKatuosoite (tulosjoukko.getString("Katuosoite"));
				asiakasOlio.setPaikkakunta (tulosjoukko.getString("Paikkakunta"));
				asiakasOlio.setPostinumero (tulosjoukko.getString("Postinumero"));
				asiakasOlio.setMaa (tulosjoukko.getString("Maa"));
			}
			
		}catch (SQLException e) {
			throw e;
		}
		// palautetaan asiakasolio
		
		return asiakasOlio;
	}
	/*
	Lis�t��n asiakkaan tiedot tietokantaan.
	Metodissa "asiakasolio kirjoittaa tietonsa tietokantaan".
	*/
     public int lisaaAsiakas (Connection connection) throws SQLException, Exception { // tietokantayhteys v�litet��n parametrina
		// haetaan tietokannasta asiakasta, jonka asiakas_id = olion id -> ei voi lis�t�, jos on jo kannassa
		String sql = "SELECT Asiakasnumero" 
					+ " FROM Asiakas WHERE Asiakasnumero = ?"; // ehdon arvo asetetaan j�ljemp�n�
		ResultSet tulosjoukko = null;
		PreparedStatement lause = null; 
		
		try {
			// luo PreparedStatement-olio sql-lauseelle
			lause = connection.prepareStatement(sql);
			lause.setInt( 1, getAsiakasnumero()); // asetetaan where ehtoon (?) arvo, olion asiakasid
			// suorita sql-lause
			tulosjoukko = lause.executeQuery();	
			if (tulosjoukko.next () == true) { // asiakas loytyi
				throw new Exception("Asiakas on jo olemassa");
			}
		} catch (SQLException se) {
            // SQL virheet
                    throw se;
                } catch (Exception e) {
            // JDBC virheet
                    throw e;
		}
		// parsitaan INSERT
		sql = "INSERT INTO Asiakas "
		+ "(Asiakasnumero, Sukunimi, Etunimi, Puhelinnumero, Katuosoite, Paikkakunta, Postinumero, Maa) "
		+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		// System.out.println("Lisataan " + sql);
		lause = null;
		try {
			// luo PreparedStatement-olio sql-lauseelle
			lause = connection.prepareStatement(sql);
			// laitetaan arvot INSERTtiin

			/*asiakasOlio.setAsiakasnumero (tulosjoukko.getInt("Asiakasnumero"));
				asiakasOlio.setSukunimi (tulosjoukko.getString("Sukunimi"));
				asiakasOlio.setEtunimi(tulosjoukko.getString("Etunimi"));
				asiakasOlio.setPuhelinnumero (tulosjoukko.getString("Puhelinnumero"));
				asiakasOlio.setKatuosoite (tulosjoukko.getString("Katuosoite"));
				asiakasOlio.setPaikkakunta (tulosjoukko.getString("Paikkakunta"));
				asiakasOlio.setPostinumero (tulosjoukko.getString("Postinumero"));
				asiakasOlio.setMaa (tulosjoukko.getString("Maa"));*/

			lause.setInt(1, getAsiakasnumero());
			lause.setString(2, getSukunimi()); 
			lause.setString(3, getEtunimi()); 
			lause.setString(4, getPuhelinnumero());
			lause.setString(5, getKatuosoite ());
			lause.setString(6, getPaikkakunta ());
			lause.setString(7, getPostinumero ());
			lause.setString(8, getMaa ());
			// suorita sql-lause
			int lkm = lause.executeUpdate();	
		//	System.out.println("lkm " + lkm);
			if (lkm == 0) {
				throw new Exception("Asiakaan lisaaminen ei onnistu");
			}
		} catch (SQLException se) {
            // SQL virheet
            throw se;
        } catch (Exception e) {
            // JDBC ym. virheet
            throw e;
		}
		return 0;
	}
	/*
	Muutetaan asiakkaan tiedot tietokantaan id-tietoa (avain) lukuunottamatta. 
	Metodissa "asiakasolio muuttaa tietonsa tietokantaan".
	*/
    public int muutaAsiakas (Connection connection) throws SQLException, Exception { // tietokantayhteys v�litet��n parametrina
		// haetaan tietokannasta asiakasta, jonka asiakas_id = olion id, virhe, jos ei l�ydy
		String sql = "SELECT Asiakasnumero" 
					+ " FROM Asiakas WHERE Asiakasnumero = ?"; // ehdon arvo asetetaan j�ljemp�n�
		ResultSet tulosjoukko = null;
		PreparedStatement lause = null;
		try {
			// luo PreparedStatement-olio sql-lauseelle
			lause = connection.prepareStatement(sql);
			lause.setInt( 1, getAsiakasnumero()); // asetetaan where ehtoon (?) arvo
			// suorita sql-lause
			tulosjoukko = lause.executeQuery();	
			if (tulosjoukko.next () == false) { // asiakasta ei l�ytynyt
				throw new Exception("Asiakasta ei loydy tietokannasta");
			}
		} catch (SQLException se) {
            // SQL virheet
                    throw se;
                } catch (Exception e) {
            // JDBC virheet
                    throw e;
		}
		// parsitaan Update, p�ivite��n tiedot lukuunottamatta avainta
		sql = "UPDATE  Asiakas "
		+ "SET Sukunimi = ?, Etunimi = ?, Puhelinnumero = ?, Katuosoite = ?, Paikkakunta = ?, Postinumero = ?, Maa = ? "
		+ " WHERE Asiakasnumero = ?";
		
		lause = null;
		try {
			// luo PreparedStatement-olio sql-lauseelle
			lause = connection.prepareStatement(sql);
			
			// laitetaan olion attribuuttien arvot UPDATEen
			
			lause.setString(1, getSukunimi()); 
			lause.setString(2, getEtunimi()); 
			lause.setString(3, getPuhelinnumero());
			lause.setString(4, getKatuosoite ());
			lause.setString(5, getPaikkakunta ());
			lause.setString(6, getPostinumero ());
			lause.setString(7, getMaa ());
			// where-ehdon arvo
            lause.setInt( 8, getAsiakasnumero());
			// suorita sql-lause
			int lkm = lause.executeUpdate();	
			if (lkm == 0) {
				throw new Exception("Asiakkaan muuttaminen ei onnistu");
			}
		} catch (SQLException se) {
            // SQL virheet
                throw se;
        } catch (Exception e) {
            // JDBC ym. virheet
                throw e;
		}
		return 0; // toiminto ok
	}
	/*
	Poistetaan asiakkaan tiedot tietokannasta. 
	Metodissa "asiakasolio poistaa tietonsa tietokannasta".
	*/
	public int poistaAsiakas (Connection connection) throws SQLException, Exception { // tietokantayhteys v�litet��n parametrina
		
		// parsitaan DELETE
		String sql = "DELETE FROM  Asiakas WHERE Asiakasnumero = ?";
		PreparedStatement lause = null;
		try {
			// luo PreparedStatement-olio sql-lauseelle
			lause = connection.prepareStatement(sql);
			// laitetaan arvot DELETEn WHERE-ehtoon
			lause.setInt( 1, getAsiakasnumero());
			// suorita sql-lause
			int lkm = lause.executeUpdate();	
			if (lkm == 0) {
				throw new Exception("Asiakaan poistaminen ei onnistu");
			}
			} catch (SQLException se) {
            // SQL virheet
                throw se;
             } catch (Exception e) {
            // JDBC virheet
                throw e;
		}
		return 0; // toiminto ok
	}
}