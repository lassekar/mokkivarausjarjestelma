package majoitusvarausjarjestelma;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class frmToimipiste extends JFrame{
	
	//m��ritell��n k�ytt�liittym�n objektit
	private JLabel lblValikko;
	private JButton btnUusiToimip;
	private JButton btnMuokkaaToimip;
	private JButton btnPoistaToimip;
	private JButton btnTakaisin;
	private JButton btnSelaaToimip;
	private JTextArea areaToimip;
	private JPanel pn1;
	private JSeparator sep;
	private JScrollPane areaWithScroll;
	private JFrame f;
	
	public frmToimipiste() {
		tuoVieKomponentitPaikalleen();
		
		
		
		//lis�t��n lopetuspainikkeelle kuuntelija
				btnTakaisin.addActionListener(
						new ActionListener() {
							public void actionPerformed (ActionEvent actEvent) {
								setVisible(false);
							}
						});
				
				btnSelaaToimip.addActionListener(
						new ActionListener() {
							public void actionPerformed (ActionEvent actEvent) {
								
							}
						});
				
				
		add(pn1);
		setLocation (100,100);//ikkunan paikka
		setSize(900, 600); //ikkunan koko
		setTitle("Majoitusvarausj�rjestelm�");
		setDefaultCloseOperation (JFrame.DISPOSE_ON_CLOSE); //Frame sulkeutuu, mutta ohjelma silti j�� k�yntiin
		
	}
	
	private void tuoVieKomponentitPaikalleen() {
		//komponenttien luonti loppuun
		this.lblValikko = new JLabel("TOIMIPISTEET"); 
		this.btnUusiToimip = new JButton("Luo uusi toimipiste");
		this.btnMuokkaaToimip = new JButton("Muokkaa toimipistett�");
		this.btnPoistaToimip = new JButton("Poista toimipiste");
		this.btnTakaisin = new JButton("Takaisin");
		this.btnSelaaToimip = new JButton("Selaa toimipisteit�");
		this.areaToimip = new JTextArea();
		this.areaToimip.setRows(20);
		this.sep = new JSeparator(SwingConstants.HORIZONTAL);
		this.pn1 = new JPanel();
		this.pn1.setLayout(new GridBagLayout());
		this.areaWithScroll = new JScrollPane(areaToimip);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		/*OHJEITA:
		 *gridx = m��ritt�� komponentin paikan rivill� eli sarakkeen sill� rivill�
		 *gridy = m��ritt�� sen mill� rivill� komponentti on
		 *weightx = m��ritt�� miten extra tila jaetaan paneelissa
		 *gridwidth = m��ritt��, kuinka pitk�lle komponentti vaakasuoraan menee rivill�
		 *ipady = lis�� sis�ist� "t�ytett�" eng. internal padding
		 *insets = lis�� m��rittelyn verran (yl�s, vasemmalle, alas, oikealle) "tyhj��"*/
		
		//Valikon painikkeet
		gbc.gridwidth = 2;
		gbc.weightx = 4;
		gbc.gridx = 0;
		gbc.gridy = 0;
		pn1.add(lblValikko,gbc);
		
		gbc.weightx = 1;
		gbc.gridx = 2;
		pn1.add(btnUusiToimip,gbc);
		
		gbc.gridx = 4;
		pn1.add(btnMuokkaaToimip,gbc);
		
		gbc.gridx = 6;
		pn1.add(btnPoistaToimip,gbc);
		
		gbc.gridx = 8;
		pn1.add(btnTakaisin,gbc);

		gbc.ipady = 40;
		gbc.weightx = 0;
		gbc.gridwidth = 12;
		gbc.gridx = 0;
		gbc.gridy = 1;
		pn1.add(sep,gbc);
		
		
		gbc.ipady = 0;
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 2;
		pn1.add(btnSelaaToimip,gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 8;
		gbc.insets = new Insets(10,0,0,0);
		pn1.add(areaWithScroll, gbc);
		
	
		
	}
	public static void main(String[] args) {
		//avataan frame
		frmToimipiste frm = new frmToimipiste();
		frm.setVisible(true);
	}

}