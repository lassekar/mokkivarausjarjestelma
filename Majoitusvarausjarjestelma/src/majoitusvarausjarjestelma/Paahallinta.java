package majoitusvarausjarjestelma;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class Paahallinta extends JFrame{
	
	//m��ritell��n k�ytt�liittym�n objektit
	private JLabel lblHallintaSovellus;
	private JLabel lblValikko;
	private JLabel lblLuoUusiMajVar;
	private JLabel lblMajVar;
	private JLabel lblValitseToimip;
	private JLabel lblValitseMokki;
	private JLabel lblValitseAsiakas;
	private JLabel lblHaeAsiakas;
	private JLabel lblTarkastaAsiakas;
	private JLabel lblUusiMajVar;
	private JLabel lblHinta;
	private JLabel lblAlkamispaiva;
	private JLabel lblPaattymispaiva;
	private JButton btnToimipisteet;
	private JButton btnPalvelut;
	private JButton btnAsiakkaat;
	private JButton btnExit;
	private JButton btnMajVar;
	private JButton btnRaportit;
	private JButton btnHae;
	private JButton btnUusiAsiakas;
	private JButton btnMuokkaaAsiakas;
	private JButton btnValitseToimip;
	private JButton btnValitseMokki;
	private JButton btnValitseAsiakas;
	private JButton btnVaraa;
	private JTextField txtHakusana;
	private JTextField txtAsiakasHaettu;
	private JTextField txtHinta;
	private JComboBox<String> cmbHakuehto;
	private JComboBox<String> cmbToimipiste;
	private JComboBox<String> cmbMokki;
	private JComboBox<String> cmbAsiakas;
	private JComboBox<String> cmbVuosiAlk;
	private JComboBox<String> cmbKKAlk;
	private JComboBox<String> cmbPaivaAlk;
	private JComboBox<String> cmbVuosiPaattyen;
	private JComboBox<String> cmbKKPaattyen;
	private JComboBox<String> cmbPaivaPaattyen;
	private JPanel pn1;
	private JSeparator sep;
	private JSeparator sep1;
	
	public Paahallinta() {
		tuoVieKomponentitPaikalleen();
		
		
		
		//lis�t��n lopetuspainikkeelle kuuntelija
				btnExit.addActionListener(
						new ActionListener() {
							public void actionPerformed (ActionEvent actEvent) {
								if (JOptionPane.showConfirmDialog(null, "Haluatko todella lopettaa?") == 0) {
									System.exit(0);
								}
							}
						});
				btnToimipisteet.addActionListener(
						new ActionListener() {
							public void actionPerformed (ActionEvent actEvent) {
								frmToimipiste frm = new frmToimipiste();
								frm.setVisible(true);
								
							}
						});
				
				btnAsiakkaat.addActionListener(
						new ActionListener() {
							public void actionPerformed (ActionEvent actEvent) {
								AsiakasGUI frm = new AsiakasGUI();
								frm.setVisible(true);
								
							}
						});
				
				//Kun k�ytt�j� on valinnut toimipisteen, ohjema hakisi tietokannasta toimipisteell� olevat m�kit
				//haun j�lkeen cmbMokki ja btnValitseMokki avautuu painettavaksi
				btnValitseToimip.addActionListener(
						new ActionListener() {
							public void actionPerformed (ActionEvent actEvent) {
								cmbMokki.setEnabled(true);
								btnValitseMokki.setEnabled(true);
								
								
							}
						});
				//Kun k�ytt�j� on valinnut m�kin, ohjema hakisi tietokannasta asiakkaat
				//haun j�lkeen cmbAsiakas ja btnValitseAsiakas avautuu painettavaksi
				btnValitseMokki.addActionListener(
						new ActionListener() {
							public void actionPerformed (ActionEvent actEvent) {
								cmbAsiakas.setEnabled(true);
								btnValitseAsiakas.setEnabled(true);
								
								
							}
						});
				//Kun k�ytt�j� on valinnut asiakkaan, loput varauksen jutuista avautuu
				btnValitseAsiakas.addActionListener(
						new ActionListener() {
							public void actionPerformed (ActionEvent actEvent) {
								cmbPaivaAlk.setEnabled(true);
								cmbKKAlk.setEnabled(true);
								cmbVuosiAlk.setEnabled(true);
								cmbPaivaPaattyen.setEnabled(true);
								cmbKKPaattyen.setEnabled(true);
								cmbVuosiPaattyen.setEnabled(true);
								txtHinta.setEnabled(true);
								btnVaraa.setEnabled(true);
								
								
							}
						});
				
				
		add(pn1);
		setLocation (100,100);//ikkunan paikka
		setSize(900, 600); //ikkunan koko
		setTitle("Majoitusvarausj�rjestelm�");
		setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE); //Ohjelma sulkeutuu
		
	}
	
	private void tuoVieKomponentitPaikalleen() {
		//komponenttien luonti loppuun
		this.lblHallintaSovellus = new JLabel("Hallintasovellus");
		this.lblValikko = new JLabel("P��VALIKKO"); 
		this.lblLuoUusiMajVar = new JLabel("2. Luo uusi majoitusvaraus");
		this.lblUusiMajVar = new JLabel("UUSI MAJOITUSVARAUS");
		this.lblMajVar = new JLabel("MAJOITUSVARAUKSET");
		this.lblValitseToimip = new JLabel("Valitse toimipiste:");
		this.lblValitseMokki = new JLabel("Valitse m�kki:");
		this.lblValitseAsiakas = new JLabel("Valitse asiakas:");
		this.lblHaeAsiakas = new JLabel("Hae asiakas: ");
		this.lblTarkastaAsiakas = new JLabel("1. Tarkasta ensin asiakkaan tiedot");
		this.lblHinta = new JLabel("Anna hinta:");
		this.lblAlkamispaiva = new JLabel("Alkamisp�iv�: ");
		this.lblPaattymispaiva = new JLabel("P��ttymisp�iv�: ");
		this.btnToimipisteet = new JButton("Toimipisteet");
		this.btnPalvelut = new JButton("Palvelut");
		this.btnAsiakkaat = new JButton("Asiakkaat");
		this.btnExit = new JButton("Lopeta");
		this.btnMajVar = new JButton("Hallitse majoitusvarauksia");
		this.btnRaportit = new JButton("Raportit");
		this.btnHae = new JButton("Hae");
		this.btnUusiAsiakas = new JButton("Luo uusi asiakas");
		this.btnMuokkaaAsiakas = new JButton("Muokkaa asiakkaan tietoja");
		this.btnValitseToimip = new JButton("Valitse");
		this.btnValitseMokki = new JButton("Valitse");
		this.btnValitseAsiakas = new JButton("Valitse");
		this.btnVaraa = new JButton("Varaa");
		this.txtHakusana = new JTextField(10);
		this.txtAsiakasHaettu = new JTextField(12);
		this.txtHinta = new JTextField(10);
		this.cmbHakuehto = new JComboBox<String>();
		this.cmbHakuehto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"Asiakasnumerolla", "Sukunimell�"}));
		this.cmbToimipiste = new JComboBox<String>();
		this.cmbToimipiste.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {}));
		this.cmbMokki = new JComboBox<String>();
		this.cmbMokki.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {}));
		this.cmbAsiakas = new JComboBox<String>();
		this.cmbAsiakas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {}));
		this.cmbVuosiAlk = new JComboBox<String>();
		this.cmbVuosiAlk.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"Vuosi"}));
		this.cmbKKAlk = new JComboBox<String>();
		this.cmbKKAlk.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"Kuukausi"}));
		this.cmbPaivaAlk = new JComboBox<String>();
		this.cmbPaivaAlk.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"P�iv�"}));
		this.cmbVuosiPaattyen = new JComboBox<String>();
		this.cmbVuosiPaattyen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"Vuosi"}));
		this.cmbKKPaattyen = new JComboBox<String>();
		this.cmbKKPaattyen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"Kuukausi"}));
		this.cmbPaivaPaattyen = new JComboBox<String>();
		this.cmbPaivaPaattyen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] {"P�iv�"}));
		this.sep = new JSeparator(SwingConstants.HORIZONTAL);
		this.sep1 = new JSeparator(SwingConstants.HORIZONTAL);
		this.pn1 = new JPanel();
		this.pn1.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		//asetetaan komponentit ei-saataville jotka on alkutilanteessa tarkoitus olla niin k�ytt�j�n virheiden v�ltt�miseksi
		this.txtAsiakasHaettu.setEnabled(false);
		this.cmbMokki.setEnabled(false);
		this.cmbAsiakas.setEnabled(false);
		this.cmbPaivaAlk.setEnabled(false);
		this.cmbKKAlk.setEnabled(false);
		this.cmbVuosiAlk.setEnabled(false);
		this.cmbPaivaPaattyen.setEnabled(false);
		this.cmbKKPaattyen.setEnabled(false);
		this.cmbVuosiPaattyen.setEnabled(false);
		this.btnValitseMokki.setEnabled(false);
		this.btnValitseAsiakas.setEnabled(false);
		this.txtHinta.setEnabled(false);
		this.btnVaraa.setEnabled(false);
		
		/*OHJEITA:
		 *gridx = m��ritt�� komponentin paikan rivill� eli sarakkeen sill� rivill�
		 *gridy = m��ritt�� sen mill� rivill� komponentti on
		 *weightx = m��ritt�� miten extra tila jaetaan paneelissa
		 *gridwidth = m��ritt��, kuinka pitk�lle komponentti vaakasuoraan menee rivill�
		 *ipady = lis�� sis�ist� "t�ytett�" eng. internal padding
		 *insets = lis�� m��rittelyn verran (yl�s, vasemmalle, alas, oikealle) "tyhj��"*/
		
		//Valikon painikkeet
		gbc.gridwidth = 2;
		gbc.weightx = 4;
		gbc.gridx = 0;
		gbc.gridy = 0;
		pn1.add(lblValikko,gbc);
		
		gbc.weightx = 1;
		gbc.gridx = 2;
		pn1.add(btnToimipisteet,gbc);
		
		gbc.gridx = 4;
		pn1.add(btnPalvelut,gbc);
		
		gbc.gridx = 6;
		pn1.add(btnAsiakkaat,gbc);
		
		gbc.gridx = 8;
		pn1.add(btnRaportit,gbc);
		
		gbc.gridx = 10;
		pn1.add(btnExit,gbc);

		gbc.ipady = 40;
		gbc.weightx = 0;
		gbc.gridwidth = 12;
		gbc.gridx = 0;
		gbc.gridy = 1;
		pn1.add(sep,gbc);
		
		//majoitusvarausten hallinta-valikko
		gbc.ipady = 0;
		gbc.weightx = 4;
		gbc.gridwidth = 1;
		gbc.gridy = 2;
		pn1.add(lblMajVar,gbc);
		
		gbc.ipady = 40;
		gbc.weightx = 0;
		gbc.gridwidth = 12;
		gbc.gridy = 3;
		pn1.add(sep1,gbc);
		
		gbc.ipady = 0;
		gbc.weightx = 1;
		gbc.gridwidth = 2;
		
		gbc.insets = new Insets(0,0,0,0);
		gbc.gridx = 2;
		gbc.gridy = 2;
		pn1.add(btnMajVar,gbc);
		
		gbc.insets = new Insets(0,0,0,0);
		gbc.gridx = 4;
		pn1.add(btnUusiAsiakas,gbc);
		
		gbc.gridx = 6;
		pn1.add(btnMuokkaaAsiakas,gbc);
		
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 5;
		pn1.add(lblUusiMajVar,gbc);
		
		//Tarkastetaan onko asiakas tietokannassa, jotta voidaan tehd� uusi varaus
		gbc.insets = new Insets(10,0,0,0);
		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = 6;
		pn1.add(lblTarkastaAsiakas,gbc);
		
		gbc.gridy = 7;
		pn1.add(lblHaeAsiakas,gbc);
		
		gbc.gridwidth = 2;
		gbc.insets = new Insets(0,0,0,0);
		gbc.gridx = 2;
		pn1.add(cmbHakuehto,gbc);
	
		gbc.insets = new Insets(0,10,0,10);
		gbc.gridx = 4;
		gbc.gridwidth = 4;
		pn1.add(txtHakusana,gbc);
	
		gbc.insets = new Insets(0,0,0,0);
		gbc.gridwidth = 2;
		gbc.gridx = 8;
		pn1.add(btnHae,gbc);
		
		gbc.insets = new Insets(10,0,0,0);
		gbc.gridy = 8;
		gbc.gridx = 2;
		gbc.gridwidth = 8;
		pn1.add(txtAsiakasHaettu,gbc);
		
		//Aloitetaan uuden varauksen luonti
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 9;
		pn1.add(lblLuoUusiMajVar,gbc);
		
		gbc.gridy = 10;
		pn1.add(lblValitseToimip,gbc);
		
		gbc.gridy = 11;
		pn1.add(lblValitseMokki,gbc);
		
		gbc.gridy = 12;
		pn1.add(lblValitseAsiakas,gbc);
		
		gbc.gridy = 13;
		pn1.add(lblAlkamispaiva,gbc);
		
		gbc.gridy = 14;
		pn1.add(lblPaattymispaiva,gbc);
		
		gbc.gridy = 15;
		pn1.add(lblHinta,gbc);
		
		gbc.gridx = 2;
		gbc.gridwidth = 4;
		gbc.gridy = 10;
		pn1.add(cmbToimipiste,gbc);
		
		gbc.gridy = 11;
		pn1.add(cmbMokki,gbc);
		
		gbc.gridy = 12;
		pn1.add(cmbAsiakas,gbc);
		
		gbc.insets = new Insets(10,0,0,0);
		gbc.gridwidth = 1;
		gbc.gridy = 13;
		pn1.add(cmbPaivaAlk,gbc);
		
		gbc.insets = new Insets(10,10,0,0);
		gbc.gridwidth = 2;
		gbc.gridx = 3;
		gbc.gridy = 13;
		pn1.add(cmbKKAlk,gbc);
		
		gbc.gridwidth = 1;
		gbc.gridx = 5;
		gbc.gridy = 13;
		pn1.add(cmbVuosiAlk,gbc);
		
		gbc.insets = new Insets(10,0,0,0);
		gbc.gridx = 2;
		gbc.gridy = 14;
		pn1.add(cmbPaivaPaattyen,gbc);
		
		gbc.insets = new Insets(10,10,0,0);
		gbc.gridwidth = 2;
		gbc.gridx = 3;
		gbc.gridy = 14;
		pn1.add(cmbKKPaattyen,gbc);
		
		gbc.gridwidth = 1;
		gbc.gridx = 5;
		gbc.gridy = 14;
		pn1.add(cmbVuosiPaattyen,gbc);
		
		gbc.insets = new Insets(10,0,0,0);
		gbc.gridwidth = 2;
		gbc.gridx = 2;
		gbc.gridy = 15;
		pn1.add(txtHinta,gbc);
		
		gbc.gridx = 8;
		gbc.gridwidth = 2;
		gbc.gridy = 10;
		pn1.add(btnValitseToimip,gbc);
		
		gbc.gridy = 11;
		pn1.add(btnValitseMokki,gbc);
		
		gbc.gridy = 12;
		pn1.add(btnValitseAsiakas,gbc);
		
		gbc.gridy = 15;
		pn1.add(btnVaraa,gbc);
		
	}
	public static void main(String[] args) {
		//avataan frame
		Paahallinta frm = new Paahallinta();
		frm.setVisible(true);
	}

}
