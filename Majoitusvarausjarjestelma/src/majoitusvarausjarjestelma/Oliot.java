package majoitusvarausjarjestelma;

public class Oliot{

	public class Asiakas {
		
		private int asiakasnumero;
		private String etunimi;
		private String sukunimi;
		private String puhelinnumero;
		private String katuosoite;
		private String paikkakunta;
		private String postinumero;
		private String maa;
				
		public Asiakas(){
			
		}
		
		public int getAsiakasnumero() {
			return asiakasnumero;
		}

		public void setAsiakasnumero(int asiakasnumero) {
			this.asiakasnumero = asiakasnumero;
		}

		public String getEtunimi() {
			return etunimi;
		}

		public void setEtunimi(String etunimi) {
			this.etunimi = etunimi;
		}

		public String getSukunimi() {
			return sukunimi;
		}

		public void setSukunimi(String sukunimi) {
			this.sukunimi = sukunimi;
		}

		public String getPuhelinnumero() {
			return puhelinnumero;
		}

		public void setPuhelinnumero(String puhelinnumero) {
			this.puhelinnumero = puhelinnumero;
		}

		public String getKatuosoite() {
			return katuosoite;
		}

		public void setKatuosoite(String katuosoite) {
			this.katuosoite = katuosoite;
		}

		public String getPaikkakunta() {
			return paikkakunta;
		}

		public void setPaikkakunta(String paikkakunta) {
			this.paikkakunta = paikkakunta;
		}

		public String getPostinumero() {
			return postinumero;
		}

		public void setPostinumero(String postinumero) {
			this.postinumero = postinumero;
		}

		public String getMaa() {
			return maa;
		}

		public void setMaa(String maa) {
			this.maa = maa;
		}
		
		public String toString() {
			return asiakasnumero + ", " + etunimi + ", " + sukunimi + ", " + puhelinnumero + ", " + katuosoite + ", " + paikkakunta + ", " + postinumero + ", " + maa;
		}
	}
	
	public class Mokki {
		
		private int mokkiId;
		private int palveluId;
		private int toimipisteId;
		private String katuosoite;
		private String kapasiteetti;
		private String postinumero;
		private String paikkakunta;
		private double hinta;
		private String nimi;
		private String koordinaatit;
		
		public Mokki() {
			
		}

		public int getMokkiId() {
			return mokkiId;
		}

		public void setMokkiId(int mokkiId) {
			this.mokkiId = mokkiId;
		}

		public int getPalveluId() {
			return palveluId;
		}

		public void setPalveluId(int palveluId) {
			this.palveluId = palveluId;
		}

		public int getToimipisteId() {
			return toimipisteId;
		}

		public void setToimipisteId(int toimipisteId) {
			this.toimipisteId = toimipisteId;
		}

		public String getKatuosoite() {
			return katuosoite;
		}

		public void setKatuosoite(String katuosoite) {
			this.katuosoite = katuosoite;
		}

		public String getKapasiteetti() {
			return kapasiteetti;
		}

		public void setKapasiteetti(String kapasiteetti) {
			this.kapasiteetti = kapasiteetti;
		}

		public String getPostinumero() {
			return postinumero;
		}

		public void setPostinumero(String postinumero) {
			this.postinumero = postinumero;
		}

		public String getPaikkakunta() {
			return paikkakunta;
		}

		public void setPaikkakunta(String paikkakunta) {
			this.paikkakunta = paikkakunta;
		}

		public double getHinta() {
			return hinta;
		}

		public void setHinta(double hinta) {
			this.hinta = hinta;
		}

		public String getNimi() {
			return nimi;
		}

		public void setNimi(String nimi) {
			this.nimi = nimi;
		}

		public String getKoordinaatit() {
			return koordinaatit;
		}

		public void setKoordinaatit(String koordinaatit) {
			this.koordinaatit = koordinaatit;
		}
		
		public String toString() {
			return mokkiId + ", " + palveluId + ", " + toimipisteId + ", " + katuosoite + ", " + kapasiteetti + ", " + postinumero + ", " + paikkakunta + ", " + hinta + ", " + nimi + ", " + koordinaatit;
		}
	}
	
	public class Raportti {
 
		private int raporttiId;
		private String alkaa;
		private String paattyy;
		
		public Raportti() {
			
		}

		public int getRaporttiId() {
			return raporttiId;
		}

		public void setRaporttiId(int raporttiId) {
			this.raporttiId = raporttiId;
		}

		public String getAlkaa() {
			return alkaa;
		}

		public void setAlkaa(String alkaa) {
			this.alkaa = alkaa;
		}

		public String getPaattyy() {
			return paattyy;
		}

		public void setPaattyy(String paattyy) {
			this.paattyy = paattyy;
		}
		
		public String toString() {
			return raporttiId + ", " + alkaa + ", " + paattyy;
		}
	}
	
	public class Palvelu {
 
		private int palveluId;
		private double hinta;
		private int asiakasnumero;
		private int raporttiId;
		
		public Palvelu() {
			
		}

		public int getPalveluId() {
			return palveluId;
		}

		public void setPalveluId(int palveluId) {
			this.palveluId = palveluId;
		}

		public double getHinta() {
			return hinta;
		}

		public void setHinta(double hinta) {
			this.hinta = hinta;
		}

		public int getAsiakasnumero() {
			return asiakasnumero;
		}

		public void setAsiakasnumero(int asiakasnumero) {
			this.asiakasnumero = asiakasnumero;
		}

		public int getRaporttiId() {
			return raporttiId;
		}

		public void setRaporttiId(int raporttiId) {
			this.raporttiId = raporttiId;
		}
		
		public String toString() {
			return palveluId + ", " + hinta + ", " + asiakasnumero + ", " + raporttiId;
		}    
	}
	
	public class Toimipiste {

		private int toimipisteId;
		private String paikkakunta;
		private String postinumero;
		private String katuosoite;
		private String koordinaatit;
		
		public Toimipiste() {
			
		}

		public int getToimipisteId() {
			return toimipisteId;
		}

		public void setToimipisteId(int toimipisteId) {
			this.toimipisteId = toimipisteId;
		}

		public String getPaikkakunta() {
			return paikkakunta;
		}

		public void setPaikkakunta(String paikkakunta) {
			this.paikkakunta = paikkakunta;
		}

		public String getPostinumero() {
			return postinumero;
		}

		public void setPostinumero(String postinumero) {
			this.postinumero = postinumero;
		}

		public String getKatuosoite() {
			return katuosoite;
		}

		public void setKatuosoite(String katuosoite) {
			this.katuosoite = katuosoite;
		}

		public String getKoordinaatit() {
			return koordinaatit;
		}

		public void setKoordinaatit(String koordinaatit) {
			this.koordinaatit = koordinaatit;
		}
		
		public String toString(){
			return toimipisteId + ", " + paikkakunta + ", " + postinumero + ", " + katuosoite + ", " + koordinaatit;
		}
	}
	
	public class Lisapalvelu {
    
		private int lisapalveluId;
		private int toimipisteId;
		private int palveluId;
		private String aikavali;
		private String palvelunimi;
		private String hinta;
		private String lisatieto;
		
		public Lisapalvelu() {
			
		}

		public int getLisapalveluId() {
			return lisapalveluId;
		}

		public void setLisapalveluId(int lisapalveluId) {
			this.lisapalveluId = lisapalveluId;
		}

		public int getToimipisteId() {
			return toimipisteId;
		}

		public void setToimipisteId(int toimipisteId) {
			this.toimipisteId = toimipisteId;
		}

		public int getPalveluId() {
			return palveluId;
		}

		public void setPalveluId(int palveluId) {
			this.palveluId = palveluId;
		}

		public String getAikavali() {
			return aikavali;
		}

		public void setAikavali(String aikavali) {
			this.aikavali = aikavali;
		}

		public String getPalvelunimi() {
			return palvelunimi;
		}

		public void setPalvelunimi(String palvelunimi) {
			this.palvelunimi = palvelunimi;
		}

		public String getHinta() {
			return hinta;
		}

		public void setHinta(String hinta) {
			this.hinta = hinta;
		}

		public String getLisatieto() {
			return lisatieto;
		}

		public void setLisatieto(String lisatieto) {
			this.lisatieto = lisatieto;
		}
		
		public String toString() {
			return lisapalveluId + ", " + toimipisteId + ", " + palveluId + ", " +  aikavali + ", " + palvelunimi + ", " + hinta + ", " + lisatieto;
		}
	}
	
	public class Lasku {

		private int laskuId;
		private int palveluId;
		private int lisapalveluId;
		private String erapaiva;
		private double hinta;
		private String laskutuspaiva;
		private int viitenumero;
		
		public Lasku() {
			
		}

		public int getLaskuId() {
			return laskuId;
		}

		public void setLaskuId(int laskuId) {
			this.laskuId = laskuId;
		}

		public int getPalveluId() {
			return palveluId;
		}

		public void setPalveluId(int palveluId) {
			this.palveluId = palveluId;
		}

		public int getLisapalveluId() {
			return lisapalveluId;
		}

		public void setLisapalveluId(int lisapalveluId) {
			this.lisapalveluId = lisapalveluId;
		}

		public String getErapaiva() {
			return erapaiva;
		}

		public void setErapaiva(String erapaiva) {
			this.erapaiva = erapaiva;
		}

		public double getHinta() {
			return hinta;
		}

		public void setHinta(double hinta) {
			this.hinta = hinta;
		}

		public String getLaskutuspaiva() {
			return laskutuspaiva;
		}

		public void setLaskutuspaiva(String laskutuspaiva) {
			this.laskutuspaiva = laskutuspaiva;
		}

		public int getViitenumero() {
			return viitenumero;
		}

		public void setViitenumero(int viitenumero) {
			this.viitenumero = viitenumero;
		}
		
		public String toString() {
			return laskuId + ", " + palveluId + ", " + lisapalveluId + ", " + erapaiva + ", " + hinta + ", " + laskutuspaiva + ", " + viitenumero;
		}		
	}			
}